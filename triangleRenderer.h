#ifndef TRIANGLERENDERER_H
#define TRIANGLERENDERER_H

// Include GLEW
#include <GL/glew.h>
// Include GLFW
#include <GLFW/glfw3.h>
#include <iostream>

#include <glm/glm.hpp>
#include "glm/gtc/matrix_transform.hpp"

class TriangleRenderer
{
private:
    unsigned int shaderProgram;
    unsigned int VBO, VAO;
    float vertices[9];

public:
    TriangleRenderer(unsigned int _shaderProgram, float _vertices[9]);

    ~TriangleRenderer();

    int render(float color[4]);
};

#endif