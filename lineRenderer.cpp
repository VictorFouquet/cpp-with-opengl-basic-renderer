#include "lineRenderer.h"

LineRenderer::LineRenderer(unsigned int _shaderProgram, float pointA[3], float pointB[3], int _steps)
{
    steps = _steps;
    shaderProgram = _shaderProgram;

    float verticesX[ steps ];
    float verticesY[ steps ];
    float verticesZ[ steps ];

    float xDeltaUnit = ( pointB[0] - pointA[0] ) / steps;
    float yDeltaUnit = ( pointB[1] - pointA[1] ) / steps;
    float zDeltaUnit = ( pointB[2] - pointA[2] ) / steps;

    verticesX[0] = pointA[0];
    verticesY[0] = pointA[1];
    verticesZ[0] = pointA[2];

    for (int i = 1; i < steps; i++)
    {
        verticesX[i] = verticesX[i-1] + xDeltaUnit;
        verticesY[i] = verticesY[i-1] + yDeltaUnit;
        verticesZ[i] = verticesZ[i-1] + zDeltaUnit;
    }

    float vertices[steps *3];

    for (int i = 0; i < steps; i++)
    {
        vertices[ i * 3 ]     = verticesX[i];
        vertices[ i * 3 + 1 ] = verticesY[i];
        vertices[ i * 3 + 2]  = verticesZ[i];
    }


    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, & VBO);
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0); 
    glBindVertexArray(0); 
}

LineRenderer::~LineRenderer()
{
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
}

int LineRenderer::render(float color[4])
{
    //glEnable( GL_POINT_SMOOTH );

    glUseProgram(shaderProgram);
    
    int location = glGetUniformLocation(shaderProgram, "u_FragColor");
    int locationMat = glGetUniformLocation(shaderProgram, "u_MVP");
    glm::mat4 proj = glm::ortho(0.f, 800.f, 0.f, 600.f, -1.f, 1.f);

    if (location == -1)
    {
        std::cout << "Unable to find uniform location\n";
    }
    glUniform4f(location, color[0], color[1], color[2], color[3]);
    glUniformMatrix4fv(locationMat, 1, GL_FALSE, &proj[0][0]);

    glEnable(GL_BLEND);
    glEnable( GL_POINT_SMOOTH );

    glPointSize(2);
    
    glBindVertexArray(VAO);
    glDrawArrays(GL_POINTS, 0, steps);
    glDisable(GL_BLEND);
    glDisable( GL_POINT_SMOOTH );
    return 0;
}
