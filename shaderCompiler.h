#ifndef SHADERCOMPILER_H
#define SHADERCOMPILER_H

#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

class ShaderCompiler
{
public:
    // Shader type is 1 for vertex shader, 0 for fragment shader.
    unsigned int compileShader(std::string src, int shaderType);

    unsigned int compileShaderProgram(unsigned int vs, unsigned int fs);
};

#endif