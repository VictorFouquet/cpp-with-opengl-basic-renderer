#ifndef CIRCLECONTOURRENDERER_H
#define CIRCLECONTOURRENDERER_H

// Include GLEW
#include <GL/glew.h>
// Include GLFW
#include <GLFW/glfw3.h>
#include <iostream>
#include <math.h>

#include <glm/glm.hpp>
#include "glm/gtc/matrix_transform.hpp"

class CircleContourRenderer
{
private:
    unsigned int shaderProgram;
    unsigned int VBO, VAO;
    unsigned int nVertices;

public:
    CircleContourRenderer(unsigned int _shaderProgram, float _radius, float x, float y, float z, int nSides, float width);

    ~CircleContourRenderer();

    int render(float color[4]);
};

#endif