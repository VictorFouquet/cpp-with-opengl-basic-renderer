#include "triangleRenderer.h"

TriangleRenderer::TriangleRenderer(unsigned int _shaderProgram, float _vertices[9])
{
    shaderProgram = _shaderProgram;

    for (int i=0; i < 9; i++)
        vertices[i] = _vertices[i];

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, & VBO);
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0); 
    glBindVertexArray(0); 
}

TriangleRenderer::~TriangleRenderer()
{
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
}

int TriangleRenderer::render(float color[4])
{
    glUseProgram(shaderProgram);
    
    int location = glGetUniformLocation(shaderProgram, "u_FragColor");
    int locationMat = glGetUniformLocation(shaderProgram, "u_MVP");
    //glm::mat4 proj = glm::ortho(-2.f, 2.f, -1.5f, 1.5f, -1.f, 1.f);
    glm::mat4 proj = glm::ortho(0.f, 800.f, 0.f, 600.f, -1.f, 1.f);

    if (location == -1)
    {
        std::cout << "Unable to find uniform location\n";
    }
    glUniform4f(location, color[0], color[1], color[2], color[3]);
    glUniformMatrix4fv(locationMat, 1, GL_FALSE, &proj[0][0]);
    //4f(locationMat, proj);

    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    return 0;
}
