#ifndef POINTRENDERER_H
#define POINTRENDERER_H

// Include GLEW
#include <GL/glew.h>
// Include GLFW
#include <GLFW/glfw3.h>
#include <iostream>
#include <stdlib.h>

#include <glm/glm.hpp>
#include "glm/gtc/matrix_transform.hpp"

class LineRenderer
{
private:
    unsigned int shaderProgram;
    unsigned int VBO, VAO;
    int steps;
    float vertices[3];

public:
    LineRenderer(unsigned int _shaderProgram, float pointA[3], float pointB[3], int _steps);

    ~LineRenderer();

    int render(float color[4]);
};

#endif