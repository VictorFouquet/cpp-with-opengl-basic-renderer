#ifndef CIRCLERENDERER_H
#define CIRCLERENDERER_H

// Include GLEW
#include <GL/glew.h>
// Include GLFW
#include <GLFW/glfw3.h>
#include <iostream>
#include <math.h>

#include <glm/glm.hpp>
#include "glm/gtc/matrix_transform.hpp"

class CircleRenderer
{
private:
    unsigned int shaderProgram;
    unsigned int VBO, VAO;
    unsigned int nVertices;

public:
    CircleRenderer(unsigned int _shaderProgram, float _radius, float x, float y, float z, int nSides);

    ~CircleRenderer();

    int render(float color[4]);
};

#endif