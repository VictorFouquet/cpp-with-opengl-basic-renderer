#include "circleRenderer.h"

CircleRenderer::CircleRenderer(unsigned int _shaderProgram, float _radius, float x, float y, float z, int nSides)
{
    nVertices = nSides + 2;
    float doublePi = 2.f * M_PI;

    float circleVerticesX[ nVertices ];
    float circleVerticesY[ nVertices ];
    float circleVerticesZ[ nVertices ];

    circleVerticesX[0] = x;
    circleVerticesY[0] = y;
    circleVerticesZ[0] = z;

    for (int i = 1; i < nVertices; i++)
    {
        circleVerticesX[i] = x + (_radius * cos( i * doublePi / nSides) );
        circleVerticesY[i] = y + (_radius * sin( i * doublePi / nSides) );
        circleVerticesZ[i] = z;
    }

    float vertices[nVertices *3];

    for (int i = 0; i < nVertices; i++)
    {
        vertices[ i * 3 ]     = circleVerticesX[i];
        vertices[ i * 3 + 1 ] = circleVerticesY[i];
        vertices[ i * 3 + 2]  = circleVerticesZ[i];
    }

    shaderProgram = _shaderProgram;

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, & VBO);
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0); 
    glBindVertexArray(0); 
}

CircleRenderer::~CircleRenderer()
{
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
}

int CircleRenderer::render(float color[4])
{
    glUseProgram(shaderProgram);
    
    int location = glGetUniformLocation(shaderProgram, "u_FragColor");
    int locationMat = glGetUniformLocation(shaderProgram, "u_MVP");

    glm::mat4 proj = glm::ortho(0.f, 800.f, 0.f, 600.f, -1.f, 1.f);

    if (location == -1)
    {
        std::cout << "Unable to find uniform location\n";
    }
    
    glUniform4f(location, color[0], color[1], color[2], color[3]);
    glUniformMatrix4fv(locationMat, 1, GL_FALSE, &proj[0][0]);

    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLE_FAN, 0, nVertices);

    return 0;
}