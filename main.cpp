// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include "glm/gtc/matrix_transform.hpp"

#include "shaderCompiler.h"
#include "triangleRenderer.h"
#include "circleRenderer.h"
#include "circleContourRenderer.h"
#include "lineRenderer.h"

GLFWwindow* window;


int main( void )
{
	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		getchar();
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow( 800, 600, "Tutorial 01", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		getchar();
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	// Initialize GLEW
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

    ShaderCompiler shaderCompiler = ShaderCompiler();
    // Compile shaders
    unsigned int vertexShader = shaderCompiler.compileShader("vertexShaderSrc.vertex", 1);
    unsigned int fragmentShader = shaderCompiler.compileShader("fragmentShaderSrc.fragment", 0);
    // Compile program shaders
    unsigned int shaderProgram = shaderCompiler.compileShaderProgram(vertexShader, fragmentShader);

    // set up vertex data (and buffer(s)) and configure vertex attributes
    // ------------------------------------------------------------------
    float vertices0[] = {
         220.f, 200.f, 0.0f, // mid
         320.f, 350.f, 0.0f, // top
         340.f, 180.f, 0.0f  // right 
    };

    float vertices1[] = {
         340.f, 180.f, 0.0f, // mid
         320.f, 350.f, 0.0f, // top
         460.f, 200.f, 0.0f  // right 
    };

    float vertices2[] = {
        350.f,  90.f, 0.0f, // left  
        500.f, 350.f, 0.0f, // top
        550.f,  70.f, 0.0f, // mid
         
    }; 

    float vertices3[] = {
        550.f,  70.f, 0.f, // left  
        500.f, 350.f, 0.f, // top
        700.f,  90.f, 0.f // mid
    }; 

    float vertices4[] = {
        20.f,  70.f, 0.f, // left  
        200.f, 300.f, 0.f, // top
        220.f,  40.f, 0.f, // mid
    };

    float vertices5[] = {
        220.f,  40.f, 0.f, // left  
        200.f, 300.f, 0.f, // top
        400.f,  70.f, 0.f // mid
    }; 

    float pointAVertices[] = {
        0.f, 300.f, 0.f
    };
    float pointBVertices[] = {
        800.f,  300.f, 0.f
    };
    CircleRenderer circle = CircleRenderer(shaderProgram, 250.f, 400.f, 300.f, 0.f, 45);
    CircleContourRenderer circleContour = CircleContourRenderer(shaderProgram, 250.f, 400.f, 300.f, 0.f, 180, 5.f);

    LineRenderer line = LineRenderer(shaderProgram, pointAVertices, pointBVertices, 800);

    TriangleRenderer tris[] = {
        TriangleRenderer(shaderProgram, vertices0),
        TriangleRenderer(shaderProgram, vertices1),
        TriangleRenderer(shaderProgram, vertices2),
        TriangleRenderer(shaderProgram, vertices3),
        TriangleRenderer(shaderProgram, vertices4),
        TriangleRenderer(shaderProgram, vertices5)
    };
    int length = sizeof(tris) / sizeof(TriangleRenderer);

    float r = 0.0f;
    float inc = 0.01f;

    float color1[] = { r, 0.3f, 0.0f, 1.f };
    float color2[] = { 0.0f, r, 0.8, 1.f };
    float color3[] = { 0.6f, 0.4f, 0.1f, 1.f };
    glEnable( GL_POINT_SMOOTH );
    
    while(
        glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		glfwWindowShouldClose(window) == 0 
    )
	{
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        
        if (r > 1.0f)
            inc = -0.05f;
        else if (r < 0.f)
            inc = 0.05f;

        r += inc;
        color1[0] = r;
        color2[1] = r;

        line.render(color2);
        circle.render(color3);
        circleContour.render(color2);
        for (int i = 0; i < length; i++)
        {
            if (i % 2 == 0)
                tris[i].render(color1);
            else tris[i].render(color2);
        }


        

        glfwSwapBuffers(window);
        glfwPollEvents();
	}
    
    //glDeleteProgram(shaderProgram);
	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}
