# BASIC C++ renderer based on openGL

#### Prerequesite
cmake ^3.15

#### Build and Run

Create a build directory at the root folder

Open a terminal in the build directory

Build with ```cmake .. && make```

Run ```./BasicRenderer```